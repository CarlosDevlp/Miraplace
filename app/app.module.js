'use strict';
/*
  Módulo principal de la aplicación
*/
var p;
(function(){

  angular
       .module('Miraplace',['ngMaterial',
                          //Directivas
                            'appMaps','mn',
                          //Fábricas
                            'floatingBoxes',
                          //Controles
                            'Miraplace.browser','Miraplace.description',
                            'Miraplace.suggestion','Miraplace.theme'
                           ])
//Definición de constantes
  .constant('src', {
              browser:{
                template:'./app/components/browser/browser.tmpl.html'
              },
              description:{
                template:'./app/components/description/description.tmpl.html'
              },
              header:{
                template:'./app/components/header/header.tmpl.html'
              },              
              suggestion:{
                template:'./app/components/suggestion/suggestion.tmpl.html'
              },
              zoom:{
                template:'./app/components/zoom/zoom.tmpl.html'
              },
              images:{
                marker:'assets/img/mkrPredio1.png',
                streetView:'assets/img/streetView.svg',
                suggestion:'assets/img/suggestion.svg',
                search:'assets/img/lupa.svg'
              }
  })
  //Controlador principal
        .controller('MainController',[
        '$scope','$mdSidenav','$mdDialog','floatingBox','$http','src'
        ,function ($scope,$mdSidenav,$mdDialog,floatingBox,$http,src){ 
          p=$scope;
          /*lista de archivos*/
          $scope.src=src;
          /*Funciones del controlador*/
            //función para buscar el detalle de los predios            
            $scope.mainLayout={
              search:function (prop){
                              //MOSTRAR PRELOADER
                              //se va a indicar que un ciertos datos estarán cargando
                              $scope.preloader.setChargingElementName("propertyInfo");                    
                              //MANDAR CONSULTA AL API                      
                                //Búqueda de los detalles de cada predio
                                    $scope.propertyInfo.id=prop.id;
                                    $scope.propertyInfo.address=prop.address;
                                    $scope.apiRequest[1].data.idDir=$scope.propertyInfo.id;
                                    $http({
                                        url:$scope.apiRequest[1].url,
                                        method:"POST",
                                        data:JSON.stringify($scope.apiRequest[1].data)
                                    }).success($scope.apiRequest[1].success);

                               //la siguiente búsqueda se hace al devolver la promesa de esta                                              
                              //Búsqueda de Resoluciones de edificación
                                // $scope.apiRequest[0].data.filter0='column8[contains]'+(prop.viaType);
                                // $scope.apiRequest[0].data.filter1='column9[contains]'+(prop.name);
                                // $.ajax($scope.apiRequest[0]);
              },
              //encapsulando la función para añadir más instrucciones      
              //Browser Flotante      
              showBrowser:function(){
                $scope.componentsVisibility.browser.hidden=false;
                if(window.innerWidth>700) 
                    floatingBox.showLayout(
                          $scope.src.browser.template,
                          //(Éxito)cuando se encuentra el predio al realizar la búsqueda
                          function(ans){                    
                                try{
                                    $scope.mainLayout.search({address:ans.address,
                                                   id:ans.id
                                                   }
                                                 );
                                }catch(err){}
                          },
                          //Caso contrario: cuando no se realiza alguna búsqueda
                          function(err){
                          }
                    );
                      
              },
              //contenido de la información
              showDescription:function (){                
                      // $scope.$apply(function(){
                        $scope.componentsVisibility.description.hidden=false;        
                      // });
              },
              //arreglar los estilos de otros controles
              adjustStyle:function(){
                /*arreglar el diseño del autocompletado y select*/
                    onload=function(){
                      var autocompleteBox=document.getElementsByClassName("md-autocomplete-suggestions");
                      var mainBox=document.getElementsByTagName("body");
                      mainBox=mainBox[0];
                      for(var i in autocompleteBox){
                          if(!isNaN(i)){
                            autocompleteBox[i].style.minWidth="50px";
                            if(window.innerWidth<=700)
                              autocompleteBox[i].style.width="80%";
                          }
                      }

                      setInterval(function(){
                        mainBox.style.overflowY="hidden";                      
                      },5);  

                    };
              }
            };
        /*Objetos*/         
        //Objeto Miraflores Api
          //serie de objetos con las apis que nos darán los datos  
            $scope.apiRequest=[
              {
                //Resoluciones de edificación (Open data) (gestor Junar)
                  url:"http://miraflores.cloudapi.junar.com/datastreams/invoke/RESOL-DE-EDIFI?auth_key=4a0745eee5787f450ab9c9ed4c47cdd49cd65ff4&output=json_array#sthash.3r7jIcG5.dpuf",
                  jsonp: "callback",
                  dataType: "jsonp",
                  data:{//parámetros de búsqueda (filtros)
                            'filter0':'column8[contains]',
                            'filter1':'column9[contains]',
                            'where':'(filter0 and filter1)'
                  },
                  success: function( data ) {/*promesa*/
                      //se terminarón de cargar los datos
                      $scope.preloader.setLoadedElementName("propertyInfo");
                      try{
                          /*si el resultado fue exitoso y se obtuvo los datos 
                          del predio se ejecutará lo sgte correctamente*/   
                            var arr=data.result[0];
                          //parseando a un nuevo objeto con la información necesaria
                            $scope.propertyInfo={
                                address: arr[8]+" "+arr[9],//+" - Mnz"+arr[11]+" Lote"+arr[12], dirección del predio
                                use: arr[7],//que tipo de uso se le da al predio
                                resolution: arr[3], //resolución
                            };
                            $scope.componentsVisibility.browser.hidden=true; 
                            $scope.componentsVisibility.browser.btnCancelHidden=false;
                      }catch(err){}
                  }
              },         
              {
                //Detalle de predios
                url:'assets/php/scraping.php', 
                type:"POST",
                data:{
                        'url':"http://digital.miraflores.gob.pe:8080/MirafloresV1/detailpredios.muni?",
                        'idDir':0
                },
                success:function( response ){
                      /*DATOS DEL PREDIO*/                
                      /*La respuesta en la búsqueda anterior supone ser la correcta
                       ,entonces, no se valida,
                      */                  
                      /*
                        1.Zonificación Vecinal:
                        2.Manzana:
                        3.Lote:
                        4.Área Terreno Propio:
                        5.Área Terreno Común:
                        6.Área de Negocio:
                        7.Uso:
                        8.Tipo:
                        9.Situación:
                      */
                      //parsear la respuesta
                      try{
                      
                      $scope.maps.deleteMarkers();
                      $scope.propertyInfo.coords={
                                                  lng:response.data.pointX,
                                                  lat:response.data.pointY
                                                  };
                      $scope.propertyInfo.zonificacionVecinal=response.data.zonificacionVecinal;
                      $scope.propertyInfo.cmpManzana=response.data.cmpManzana;
                      $scope.propertyInfo.cmpLote=response.data.cmpLote;
                      $scope.propertyInfo.areaTerrenoPropio=response.data.areaTerrenoPropio;
                      $scope.propertyInfo.areaTerrenoComun=response.data.areaTerrenoComun;
                      $scope.propertyInfo.areaNegocio=response.data.areaNegocio;
                      $scope.propertyInfo.usoPredio=response.data.usoPredio;
                      $scope.propertyInfo.tipoPredio=response.data.tipoPredio;
                      $scope.propertyInfo.situacionPredio=response.data.situacionPredio;
                      //si las coordenadas son cero
                      if(response.data.pointX=="0" || response.data.pointY=="0")
                        throw "cero no es una coordenada valida";
                      //mapearlo
                      $scope.maps.addMarker(
                                              new google.maps.LatLng(
                                                                    $scope.maps.parseMCoord(response.data.pointY),
                                                                    $scope.maps.parseMCoord(response.data.pointX)
                                                                    )
                                            );  
                      $scope.maps.centerMap();//centrar el mapa al marker
                      //crear un street view con la posición del marker
                      // if(window.innerWidth>1020) 
                      //     $scope.maps.showStreetViewFrom();                      
                      }
                      catch(err){
                         console.log(response);
                      }
                      //OCULTAR PRELOADER
                        $scope.preloader.setLoadedElementName("propertyInfo");
                        $scope.componentsVisibility.browser.hidden=true; 
                        $scope.componentsVisibility.browser.btnCancelHidden=false;
                      //enseñar la descripción del predio
                        $scope.mainLayout.showDescription();
                }
              }

            ];
          //Objeto Preloader (mostrar preloader mientras los valores del objeto son falsos)
          /*Escribir poner el número de elementos a cargar de acuerdo a la aplicación*/
          $scope.preloader={
                        ready:false,//cuando la página está completamente cargada (no manda solicitudes el back)
                        elementsCharged:[],
                        init:function(){
                            /*El mapa*/
                            this.elementsCharged["maps"]=false;
                            /*información del predio sacado de la base de datos*/
                            this.elementsCharged["propertyInfo"]=true;
                        },
                        setLoadedElementName:function (elem){                        
                              this.elementsCharged[elem]=true;
                              this.ready=this.elementsReady();
                        },
                        setChargingElementName:function(elem){
                              this.elementsCharged[elem]=false;
                              this.ready=this.elementsReady();
                        },
                        elementsReady:function (){
                          /*verificar si todos los elementos están listos para esconder el preloader*/
                            var isReady=true;                            
                            for(var i in this.elementsCharged)
                                isReady=(this.elementsCharged[i] && isReady);
                            return isReady;
                        }
          };
          $scope.preloader.init();          
          /*objeto predio (el que lleva la información del predio buscado)*/
          $scope.propertyInfo={};          
          //Visibilidad de los diversos componentes de toda la aplicación
          //objetos que permitirán acceder a los componentes que la app tiene desde el módulo raiz          
          $scope.componentsVisibility={
                browser:{/*componente browser*/
                  /*Botón cancelar*/
                  hidden:true,
                  btnCancelHidden:true
                },
                description:{/*Componenente descripción*/
                  hidden:true,
                  hide:function(){
                    if($scope.maps.markers.length>0)
                       this.hidden=true;                    
                  }
                },
                suggestion:{
                  hidden:true
                }
          };      

        //Ejecución Inicial
        $scope.mainLayout.showBrowser();
        $scope.mainLayout.adjustStyle();
}
]),
 angular
       .module('Miraplace2',[]);
            
})();
