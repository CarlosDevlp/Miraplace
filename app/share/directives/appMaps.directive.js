/*Directiva de Google Maps*/
angular.module('appMaps', [])
	.directive('appMap',function(){
		// Runs during compile
		return {			 
			 restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
			 template: '<div id="maps" whole></div>',
			 controller: function($scope, $element, $attrs, $transclude, src) {
				//objeto mapa
				$scope.maps={
					self:undefined,
					properties:undefined,
					markers:[],					
					geocoder:undefined,
					streetView:undefined,
					init:function(){

							/*propiedades que tendrá el objeto Maps*/
							this.properties={
	                                center:new google.maps.LatLng(-12.1214747,-77.0285793),
	                                zoom:14,
	                                disableDefaultUI:true,//controles que vienen por defecto en el maps
	                                mapTypeId:google.maps.MapTypeId.ROADMAP
							};
							/*referenciar a un objeto maps de google maps a través de nuestra variable*/
							this.self=new google.maps.Map(document.getElementById("maps"),this.properties);
		                    /*PreLoader*/
		                    google.maps.event.addListener(this.self, 'tilesloaded', function(){

		                    	$scope.$apply(function() {
		                    		$scope.preloader.setLoadedElementName("maps");
                        		});                      
        						
    						});

		                    //crearun geodecoder para obtener coordenadas a partir de direcciones
		                    this.geocoder= new google.maps.Geocoder();
					},					
            		centerMap:function(toMarker){
            			var centerPos;            			
            			if(toMarker)//centrar el mapa hacía un marker en específico
            				centerPos=new google.maps.LatLng(toMarker.getPosition().lat() - 0.0002,toMarker.getPosition().lng());
            			else//centrar el mapa hacía el marker por defecto
							centerPos=new google.maps.LatLng(this.markers[0].getPosition().lat() - 0.0002,this.markers[0].getPosition().lng());            			
            			// centerPos.H-=0.0002;//poner el centro un poco más arriba
            			$scope.maps.self.setZoom(19);                                    
                        $scope.maps.self.setCenter(centerPos);

            		},
            		/*Solo puede haber un marker (su uso debe estar junto con deleteMarkers)*/
            		addMarker: function(location){
                      this.markers.push(new google.maps.Marker({
                                   position: location,//coordenadas del marker
                                   map:  this.self,
                                   icon: src.images.marker,//imagen que reemplaza al marker por defecto
                                   animation: google.maps.Animation.DROP
                      }));
                      this.self.setZoom(19);                      
                      this.properties.zoom=19;                      

                      this.self.setCenter(this.markers[this.markers.length-1].getPosition());

                      google.maps.event.addListener(this.markers[this.markers.length-1],'mousedown',function() {
                                   
                                     $scope.mainLayout.showDescription();
									 $scope.maps.centerMap(this);
									 $scope.$apply();
                      		
                      });
            		},
		            deleteMarkers: function(){		              
		              for (var i in this.markers) 
		                this.markers[i].setMap(null);
		              this.markers=[];		              
		            },
		            zoomIn:function(){
		              if(this.properties.zoom<21)
		                this.properties.zoom++;
		              this.self.setZoom(this.properties.zoom);              
		            },
		            zoomOut:function(){
		            if(this.properties.zoom>0)     
		                this.properties.zoom--;
		              this.self.setZoom(this.properties.zoom);              
		            },		            
		            getMarkerImage:function(mkr,tipo){
		              try{
		                return this.MarkerImage[mkr][tipo];
		              }
		              catch(err){
		                return null;
		              }
		            },
		            geocodeAddress: function(address) {

					  this.geocoder.geocode({'address': address}, function(results, status) {

						  	//si el estado del resultado es OK
						    if (status === google.maps.GeocoderStatus.OK)
						    	//crear un nuevo marker con loas coordenadas obtenidas
						        $scope.maps.addMarker(results[0].geometry.location);						    
					    
					  });
					},
					parseMCoord: function(str){
						//parsear string en una coordenada válida por google maps
						//necesario para el api de miraplace						
							var temp=str.split(",");
							temp[1]=temp[1].substring(0,7);
							return Number(temp.join("."));						
					},
					/*mostrar el street view desde la posición de un marker*/
					showStreetViewFrom: function(toMarker){
						var pos;//posición del marker
						if(toMarker!=undefined)
							pos=toMarker.getPosition();
						else
							pos=this.markers[0].getPosition();

			             this.streetView=new google.maps.StreetViewPanorama(
              							document.getElementById("streetView"),
                                        {
                                          position:pos,
                                          pov: {
                                            heading: 165,
                                            pitch: 10,
                                            zoom: 1
                                          },
                                          disableDefaultUI:true
                                          });
			              this.streetView.setVisible(true);
					
					}
			    };


				$scope.maps.init();


			 },
			
		};
	});