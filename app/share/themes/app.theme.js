/*configuración principal del tema de colores de la aplicación*/
angular.module('Miraplace.theme', [])
       .config([
    '$mdThemingProvider',
    function($mdThemingProvider){

    		$mdThemingProvider.definePalette('orangeButton', {
    												'50': 'ffa800',
    												'100':'ffa800',
        									 		'200': 'ffa800',
        									 		'300': 'ffa800',
        									 		'400' : 'ffa800',
        									 		'500' : 'ff9501',
        									 		'600' : 'ff9501',
        									 		'700' : 'ff9501',    									 		
        									 		'A100': 'ff9501',
        									 		'800' : 'ff9501',
        									 		'900' : 'ff7802',
        									 		'A200': 'ff7802',
        									 		'A400': 'ff7802',
        									 		'A700': 'ff7802',
        									 		'contrastDefaultColor': 'light',
        									 		'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
    											    'contrastLightColors': undefined  
        										}
        									 );
    		$mdThemingProvider.definePalette('greenButton', {
    												'50': '96b850',
    												'100':'96b850',
        									 		'200': '96b850',
        									 		'300': '96b850',
        									 		'400' : '96b800',
        									 		'500' : '96b800',
        									 		'600' : '96b800',
        									 		'700' : '96b800',    									 		
        									 		'A100': '96b800',
        									 		'800' : '7c9800',
        									 		'900' : '7c9800',
        									 		'A200': '7c9800',
        									 		'A400': '7c9800',
        									 		'A700': '7c9800',
        									 		'contrastDefaultColor': 'light',
        									 		'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
    											    'contrastLightColors': undefined  
        										}
        									 );
    	  /*tema por defecto*/
          $mdThemingProvider.theme('default')
                              .primaryPalette('blue')
                              	.accentPalette('red');
          /*tema de botones*/
          $mdThemingProvider.theme('buttonsTheme')
          						.primaryPalette('orangeButton')
          						 .accentPalette('greenButton');
    }]);