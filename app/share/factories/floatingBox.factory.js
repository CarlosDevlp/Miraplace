//menus flotantes y mensajes de alerta
/*
  Información:
  Callbacks:
  -se envían 2 cb  (1 Success callback, 1 Error callback).
  -a ambos cb se les responde con un JSON con el resultado exitoso o el de error.
  -en el caso de la promesa error, si la promesa no responde con un JSON, es el mensaje
   se cerró por razones que no necesariamente son de error; por lo tanto, igual se eje-
   cuta el cb.
*/
angular.module('floatingBoxes', [])
.factory('floatingBox',[
  '$mdDialog','$mdBottomSheet',
  function ($mdDialog,$mdBottomSheet){  
          return{              
              showAlert:function (title,content,btnText,cb,ev){
                          $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.body))
                              .title(title)
                              .content(content)
                              .ariaLabel('Alert Dialog Demo')
                              .ok(btnText)
                              .targetEvent(ev)
                              .escapeToClose(false)
                            ).then(//promesa
                              cb
                            );        
                },              
                showLayout:function(tmpl,cb,ecb,ev){  
                            $mdDialog.show({
                              templateUrl: tmpl,
                              parent: angular.element(document.body),
                              targetEvent: ev,
                              escapeToClose:false
                            }).then(//promesas de respuestas
                                cb,ecb
                            );
                },
                showListBottomSheet : function(tmpl,ctrl,cb,ev) {
                    $mdBottomSheet.show({
                      templateUrl: tmpl,                      
                      targetEvent: ev,
                      controller: ctrl
                    }).then(cb);
              }
          };
        

  }
]);