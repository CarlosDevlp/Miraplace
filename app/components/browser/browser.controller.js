'use strict';
//Control "Browser"
angular.module('Miraplace.browser', [])
.controller('BrowserController',[
      '$scope','$mdDialog','$q','$http'
      ,function ($scope,$mdDialog,$q,$http) {                
              /*Funciones del controlador*/
                $scope.layout={
                    /*esconder el browser*/
                    hide : function() {
                        $mdDialog.hide();
                    },
                    /*Si la acción de este formulario se puede cancelar*/
                    isCancelable : function(){
                      var cancelable=true;
                        if($scope.componentsVisibility==undefined
                          || $scope.componentsVisibility.browser.btnCancelHidden==true)
                          cancelable=false;
                      return cancelable; 
                    },
                    /*cancelar la acción que se iban a tomar*/
                    cancel : function() {
                      $scope.layout.clear();
                      $scope.componentsVisibility.browser.hidden=true;
                    },
                    /*buscar el predio con el contenido obtenido desde este mismo formulario*/
                    search : function(callback) {
                      try{
                          //validar el contenido                          
                          var response={
                                        id: $scope.addressSeeker.selectedProperty.idDir,
                                        address: $scope.addressSeeker.selectedProperty.dirCompleta
                                      }; 
                          //limpiar el browser
                          $scope.layout.clear();
                          //si se está buscando desde una caja flotante, ocultarla                      
                          $mdDialog.hide(response);
                          //sino solo enviar la respuesta
                          callback(response);                          

                      }
                      catch(err){}
                    },
                    //limpiar el formulario del browser
                    clear: function(){ 
                        $scope.viaTypes.selected="Tipo de vía";
                        $scope.addressSeeker.searchText="";
                    }
                  
                };
            /*Objetos*/
                  /*Opciones para el select de tipos de de vía*/               
                $scope.viaTypes={
                              placeHolder:"Tipo de vía",
                              selected:"Tipo de vía",
                              options:["Avenida","Calle","Jiron","Malecon","Parque","Pasaje"]
                            };                            
                  //Objeto dirección, que tiene las funciones necesarias para que funcione el autocompletado
                $scope.addressSeeker={
                              searchText:"",
                              selected:"",                              
                              selectedProperty:"",//propiedad seleccionada con el autocompletado
                              getRelatedAddresses:function (){                              
                               $scope.deferred=$q.defer();                               
                               $http({
                                        url:'assets/php/scraping.php',
                                        method:"POST",
                                        data:JSON.stringify({//parámetros de búsqueda
                                              'url':"http://digital.miraflores.gob.pe:8080/MirafloresV1/listpredios.muni?",
                                              'nombreVia':this.searchText,
                                              'codTipoVia':((isNaN($scope.viaTypes.selected))?0:$scope.viaTypes.selected),
                                        })
                                      })
                                      .success(
                                         function( response ){                                                      
                                                      var coincidences=[];
                                                      try{
                                                          if(response.data instanceof Array)                                                                
                                                                    coincidences=response.data;
                                                          else
                                                            coincidences.push(response.data);
                                                      }catch(err){
                                                        //no se encontró el predio
                                                      }
                                                      finally{                                          
                                                        $scope.deferred.resolve(coincidences);                                                        
                                                      }
                                                }

                                      );
                                  return $scope.deferred.promise;                               
                              },
                              setSelected:function(prop){                                  
                                  $scope.addressSeeker.selectedProperty=prop;
                              }
                            };


}]);