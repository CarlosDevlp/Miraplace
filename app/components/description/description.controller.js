//controlador 3 description
angular.module('Miraplace.description', [])
.controller('DescriptionController',[
 '$scope','$window','floatingBox','src'
,function ($scope,$window,floatingBox,src){	
	/*lista de archivos*/
        $scope.src=src;
	/*Funciones del controlador*/
		$scope.layout={
			/*Ocultar el formulario*/
			hide:function(){
				//si el predio tenía coordenadas, entonces se podrá mostrar el mapa
				if($scope.maps.markers.length>0)
					$scope.componentsVisibility.description.hidden=true;
			},
			/*ir al street view en otra web*/
			goToStreetView:function(){
									if($scope.property.data.coords.lat!="0" && $scope.property.data.coords.lng!="0"){
										var lat=$scope.maps.markers[0].position.lat(),lng=$scope.maps.markers[0].position.lng();
										var url="https://maps.google.com/maps?q=&layer=c&cbll="+lat+","+lng+"&cbp=11,0,0,0,0&ll="+lat+","+lng+"&z=10";
										$window.open(url);
									}
								},
			/*Ir al formulario de envío de quejas*/
			goToSuggestionForm:function(){
									$scope.componentsVisibility.suggestion.hidden=false;
								},
			goToSuggestionFloatingBoxForm:function(){
									floatingBox.showLayout($scope.src.suggestion.template,function(){},function(){});
								}
		};
	
   /*Objetos*/							
	$scope.property={
						names:[
						    "no",
						    "no",
						    "no",
		 					"Zonificación Vecinal:",
							"Manzana:",
							"Lote:",
							"Área Terreno Propio:",
							"Área Terreno Común:",
							"Área de Negocio:",
							"Uso:",
							"Tipo:",
							"Situación:"
							],
						data:$scope.propertyInfo
					};
}
]);