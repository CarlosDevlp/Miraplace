/*Controlador de Envío de quejas*/
var x;
angular.module('Miraplace.suggestion', [])
.controller('SuggestionController', [
 '$scope','floatingBox','$mdDialog','$window','$http'
,function($scope,floatingBox,$mdDialog,$window,$http){	
	/*obteniendo configuración de la aplicación*/
		$http.get("app/app.conf.json").success(function(data){
			$scope.conf=data;
		});
	/*Funciones del controlador*/
		$scope.layout={
			/*Si el formulario es una caja flotante, 
			se cancela la actividad que se iba a realizar*/
			cancel: function(){  
	            $mdDialog.cancel();
	        },
	        //ocultar el formulario
			hide:function(){
				$scope.componentsVisibility.suggestion.hidden=true;
			},
			//limpiar el contenido de la caja de texto
			clear:function(){
				$scope.message.content="";
			},
			//enviar el mensaje de queja
			send:function(){
				$window.location="mailto:"+$scope.message.email+
							 "?subject="+$scope.message.subject+
								 "&body="+$scope.message.content;
				floatingBox.showAlert("Aviso","El mensaje se ha enviado con éxito.","ok");
			}
		};
	/*Objeto 'Mensaje' del controlador*/
		$scope.message={
		      email:"",/*email receptor*/
			  subject:"",/*Asunto*/
			  content:"",/*Contenido*/

		};

	/*obteniendo configuración de la aplicación*/
	$http.get("app/app.conf.json").success(function(data){			
			$scope.message.email=data.suggestion.message.email;
			$scope.message.subject=data.suggestion.message.subject;			
	});

}
]);