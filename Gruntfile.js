module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        mangle: false
      },
      my_target: {
        files: {
          'build/script.min.js': ["./app/share/directives/appMaps.directive.js",
                                  "./app/share/directives/mnTouch.js",
                                  "./app/components/browser/browser.controller.js",
                                  "./app/components/description/description.controller.js",
                                  "./app/components/suggestion/suggestion.controller.js",
                                  "./app/share/factories/floatingBox.factory.js",
                                  "./app/share/themes/app.theme.js",
                                  "./app/app.module.js"]
        }
      }
    },
    htmlmin: {
      dist: {
          options: {
             removeComments: true,
             collapseWhitespace: true
          },
          files: {// Dictionary of files
            'build/index.html': 'index.html',
            'build/browser.html': 'app/components/browser/browser.tmpl.html',
            'build/description.html': 'app/components/description/description.tmpl.html',
            'build/header.html': 'app/components/header/header.tmpl.html',
            'build/suggestion.html': 'app/components/suggestion/suggestion.tmpl.html',
            'build/zoom.html': 'app/components/zoom/zoom.tmpl.html',
          }
      }
    },
    cssmin: {
       dist: {
          options:{
             banner: '/*Miraplace 2015!*/'
          },
          files: {
             'build/style1.min.css': ['assets/css/app.css'],
             'build/style2.min.css': ['assets/css/appPhone.css'],
             'build/style3.min.css': ['assets/css/appDesktop.css']
          }
      }
    },
    imagemin: {
       dist: {
          options: {
            optimizationLevel: 5
          },
          files: [{
             expand: true,
             cwd: 'assets/img',
             src: ['**/*.{png,svg}'],
             dest: 'build/'
          }]
       }
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js',
      },
      //continuous integration mode: run tests once in PhantomJS browser.
      continuous: {
        configFile: 'karma.conf.js',
        singleRun: true,
        browsers: ['Firefox']
      },
    }
    // karma: {
    //   unit: {
    //     configFile: 'karma.conf.js'
    //   }
    // }


  });

  // Load the plugin that provides the "uglify" task.
     grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-cssmin');
    // grunt.loadNpmTasks('grunt-contrib-htmlmin');
    // grunt.loadNpmTasks('grunt-contrib-imagemin');
    // grunt.loadNpmTasks('grunt-jasmine-runner');
    //grunt.loadNpmTasks('grunt-karma');
  // Default task(s). 'uglify','htmlmin','cssmin','imagemin'
  grunt.registerTask('default', ['uglify']);

};